$(document).foundation();

// Dropdown menu elemens
const menuIcon = document.querySelector(".burgerIcon");
const mobiNav = document.querySelector(".mobi-nav");

// Responsive Basket elements
const miniCart = document.querySelector(".desk-mini-cart");
const basket = document.querySelector(".menu-text");
const mobiMiniCart = document.querySelector(".mobi-mini-cart");
const mobiBasket = document.querySelector(".mobi-menu-text");

// Dropdown menu listeners
menuIcon.addEventListener("mouseover", toggleMobiNav);
mobiNav.addEventListener("mouseleave", toggleMobiNav);

// Responsive Basket listeners
basket.addEventListener("mouseover", toggleMiniCart);
basket.addEventListener("mouseout", toggleMiniCart);
mobiBasket.addEventListener("mouseover", toggleMiniCart);
mobiBasket.addEventListener("mouseout", toggleMiniCart);

function toggleMobiNav() {
  mobiNav.classList.toggle("hidden");
}

function toggleMiniCart() {
  miniCart.classList.toggle("hidden");
  mobiMiniCart.classList.toggle("hidden");
}
